package net.covasoft.poker;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class PokerTest {
	
	/*********************************************************************************/
	/********************* FUNCTIONAL TEST CASES *************************************/
	/**********************************************************************************/

	//@Test
	public void poker_tests() {
		//Assert.fail("Implement your test cases for the Poker class here");
	}
	
	/*
	 * This is the test case where Flush verses 3 of a kind condition
	 */
	@Test 
	public void flush_vs_threeOfAKind() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Jane";
		String player2 = "Simon";

		Card card1 = new Card(Suit.HEARTS, Rank.THREE); 
		Card card2 = new Card(Suit.HEARTS,Rank.FOUR);
		Card card3 = new Card(Suit.HEARTS, Rank.ACE); //Flush cards
		Card card4 = new Card(Suit.HEARTS, Rank.NINE); 
		Card card5 = new Card(Suit.HEARTS, Rank.QUEEN);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.FIVE); 
		Card card7 = new Card(Suit.DIAMONDS,Rank.SEVEN); 
		Card card8 = new Card(Suit.HEARTS, Rank.FIVE); // 3 of A kind cards
		Card card9 = new Card(Suit.SPADES, Rank.FIVE); 
		Card card10 = new Card(Suit.SPADES, Rank.QUEEN);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals(winnerHand.iterator().next().getPlayer(), player1);
		System.out.println("The flush vs 3 of a kind="+winnerHand.iterator().next().getPlayer());
	}
	
	/*
	 * This is the test case where Flush verses one Pair condition
	 */
	@Test 
	public void flush_vs_onePair() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Jane";
		String player2 = "Simon";
		
		Card card1 = new Card(Suit.CLUBS, Rank.ACE); // one pair card
		Card card2 = new Card(Suit.DIAMONDS,Rank.SEVEN); 
		Card card3 = new Card(Suit.HEARTS, Rank.FIVE); 
		Card card4 = new Card(Suit.SPADES, Rank.ACE); // one pair card
		Card card5 = new Card(Suit.SPADES, Rank.QUEEN);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.HEARTS, Rank.THREE); 
		Card card7 = new Card(Suit.HEARTS,Rank.FOUR);
		Card card8 = new Card(Suit.HEARTS, Rank.ACE); //Flush cards
		Card card9 = new Card(Suit.HEARTS, Rank.NINE); 
		Card card10 = new Card(Suit.HEARTS, Rank.QUEEN);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals(winnerHand.iterator().next().getPlayer(), player2);
		System.out.println("The flush vs one pair="+winnerHand.iterator().next().getPlayer());
	}
	
	/*
	 * This is the test case where Flush verses High card condition
	 */
	@Test 
	public void flush_vs_highCard() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Jane";
		String player2 = "Simon";
		
		Card card1 = new Card(Suit.HEARTS, Rank.THREE); 
		Card card2 = new Card(Suit.HEARTS,Rank.FOUR);
		Card card3 = new Card(Suit.HEARTS, Rank.ACE); //Flush cards
		Card card4 = new Card(Suit.HEARTS, Rank.NINE); 
		Card card5 = new Card(Suit.HEARTS, Rank.QUEEN);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.HEARTS, Rank.SEVEN); 
		Card card7 = new Card(Suit.SPADES, Rank.THREE);
		Card card8 = new Card(Suit.SPADES, Rank.ACE); //High value card
		Card card9 = new Card(Suit.SPADES, Rank.FOUR); 
		Card card10 = new Card(Suit.DIAMONDS, Rank.FIVE);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals(winnerHand.iterator().next().getPlayer(), player1);
		System.out.println("The flush vs high card="+winnerHand.iterator().next().getPlayer());
	}
	
	/*
	 * This is the test case where 3 of a kind verses One pair condition
	 */
	@Test 
	public void threeOfAKind_vs_onePair() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Kelly";
		String player2 = "Ryan";
		
		Card card1 = new Card(Suit.CLUBS, Rank.FIVE); 
		Card card2 = new Card(Suit.DIAMONDS,Rank.SEVEN); 
		Card card3 = new Card(Suit.HEARTS, Rank.FIVE); // 3 of A kind cards
		Card card4 = new Card(Suit.SPADES, Rank.FIVE); 
		Card card5 = new Card(Suit.SPADES, Rank.QUEEN);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.HEARTS, Rank.THREE); 
		Card card7 = new Card(Suit.SPADES,Rank.FOUR);
		Card card8 = new Card(Suit.CLUBS, Rank.QUEEN); //One pair
		Card card9 = new Card(Suit.DIAMONDS, Rank.NINE); 
		Card card10 = new Card(Suit.DIAMONDS, Rank.QUEEN); //One pair

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);		

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals(winnerHand.iterator().next().getPlayer(), player1);
		System.out.println("The 3 of a kind vs One pair="+winnerHand.iterator().next().getPlayer());
	}
	
	/*
	 * This is the test case where 3 of a kind verses High card condition
	 */
	@Test 
	public void threeOfAKind_vs_highCard() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Kelly";
		String player2 = "Ryan";
		
		Card card1 = new Card(Suit.HEARTS, Rank.THREE); 
		Card card2 = new Card(Suit.SPADES,Rank.FOUR);
		Card card3 = new Card(Suit.CLUBS, Rank.KING); //High Card
		Card card4 = new Card(Suit.DIAMONDS, Rank.NINE); 
		Card card5 = new Card(Suit.DIAMONDS, Rank.QUEEN);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);
		
		Card card6 = new Card(Suit.CLUBS, Rank.FIVE); 
		Card card7 = new Card(Suit.DIAMONDS,Rank.SEVEN); 
		Card card8 = new Card(Suit.HEARTS, Rank.FIVE); // 3 of A kind cards
		Card card9 = new Card(Suit.SPADES, Rank.FIVE); 
		Card card10 = new Card(Suit.SPADES, Rank.QUEEN);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);				

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals(winnerHand.iterator().next().getPlayer(), player2);
		System.out.println("The 3 of a kind vs high card="+winnerHand.iterator().next().getPlayer());
	}
	
	/*
	 * This is the test case where One pair verses High card condition
	 */
	@Test 
	public void onePair_vs_highCard() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Kelly";
		String player2 = "Ryan";
		
		Card card1 = new Card(Suit.HEARTS, Rank.THREE); 
		Card card2 = new Card(Suit.SPADES,Rank.FOUR);
		Card card3 = new Card(Suit.CLUBS, Rank.QUEEN); //One pair
		Card card4 = new Card(Suit.DIAMONDS, Rank.NINE); 
		Card card5 = new Card(Suit.DIAMONDS, Rank.QUEEN); //One pair

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);	
		
		Card card6 = new Card(Suit.CLUBS, Rank.FIVE); 
		Card card7 = new Card(Suit.DIAMONDS,Rank.SEVEN); 
		Card card8 = new Card(Suit.HEARTS, Rank.ACE); //High card
		Card card9 = new Card(Suit.SPADES, Rank.FIVE); 
		Card card10 = new Card(Suit.SPADES, Rank.JACK);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);			

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals(winnerHand.iterator().next().getPlayer(), player1);
		System.out.println("The One pair vs High card="+winnerHand.iterator().next().getPlayer());
	}

	/*
	 * This test case is where Player1 has all 5 cards of the same suit and wins by Flush
	 */
	@Test 
	public void when_flush_then_onePlayerWins() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Joe";
		String player2 = "Jen";

		Card card1 = new Card(Suit.HEARTS, Rank.THREE); 
		Card card2 = new Card(Suit.HEARTS,Rank.FOUR);
		Card card3 = new Card(Suit.HEARTS, Rank.ACE); //Flush cards
		Card card4 = new Card(Suit.HEARTS, Rank.NINE); 
		Card card5 = new Card(Suit.HEARTS, Rank.QUEEN);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.FIVE); 
		Card card7 = new Card(Suit.DIAMONDS,Rank.SEVEN); 
		Card card8 = new Card(Suit.HEARTS, Rank.FIVE); //All random cards
		Card card9 = new Card(Suit.SPADES, Rank.NINE); 
		Card card10 = new Card(Suit.SPADES, Rank.QUEEN);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals("The Flush inner is",winnerHand.iterator().next().getPlayer(), player1);
		System.out.println("The first flush winner is="+winnerHand.iterator().next().getPlayer());
	}

	/*
	 * This test case is where both players have all 5 cards of the same suit and 
	 * player with first highest card wins
	 */
	@Test 
	public void when_flush_both_then_firstHighestCardWins() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Joe";
		String player2 = "Jen";

		Card card1 = new Card(Suit.HEARTS, Rank.QUEEN); 
		Card card2 = new Card(Suit.HEARTS,Rank.SIX);
		Card card3 = new Card(Suit.HEARTS, Rank.JACK); 
		Card card4 = new Card(Suit.HEARTS, Rank.NINE); 
		Card card5 = new Card(Suit.HEARTS, Rank.TWO);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.FOUR); 
		Card card7 = new Card(Suit.CLUBS,Rank.KING); 
		Card card8 = new Card(Suit.CLUBS, Rank.ACE); //Highest card
		Card card9 = new Card(Suit.CLUBS, Rank.THREE); 
		Card card10 = new Card(Suit.CLUBS, Rank.TEN);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals(winnerHand.iterator().next().getPlayer(), player2);
		System.out.println("The second flush winner is="+winnerHand.iterator().next().getPlayer());
	}

	/*
	 * This test case is where both players have all 5 cards of the same suit and 
	 * one card with same strength so the player with second highest card wins
	 */
	@Test 
	public void when_flush_both_then_secondHighestCardWins() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Jim";
		String player2 = "Pam";

		Card card1 = new Card(Suit.HEARTS, Rank.JACK); // Highest card
		Card card2 = new Card(Suit.HEARTS,Rank.TWO);
		Card card3 = new Card(Suit.HEARTS, Rank.SIX); 
		Card card4 = new Card(Suit.HEARTS, Rank.FOUR); 
		Card card5 = new Card(Suit.HEARTS, Rank.QUEEN); //Same strength

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.THREE); 
		Card card7 = new Card(Suit.CLUBS,Rank.FIVE); 
		Card card8 = new Card(Suit.CLUBS, Rank.EIGHT); 
		Card card9 = new Card(Suit.CLUBS, Rank.TEN); 
		Card card10 = new Card(Suit.CLUBS, Rank.QUEEN); //Same strength

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals(winnerHand.iterator().next().getPlayer(), player1);
		System.out.println("The third flush winner is="+winnerHand.iterator().next().getPlayer());
	}

	/*
	 * This test case is where both players have all 5 cards of the same suit and 
	 * two cards with same strength so the player with third highest card wins
	 */
	@Test 
	public void when_flush_both_then_thirdHighestCardWins() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Jim";
		String player2 = "Pam";

		Card card1 = new Card(Suit.HEARTS, Rank.JACK); //Same strength
		Card card2 = new Card(Suit.HEARTS,Rank.TWO);
		Card card3 = new Card(Suit.HEARTS, Rank.SIX); 
		Card card4 = new Card(Suit.HEARTS, Rank.FOUR); 
		Card card5 = new Card(Suit.HEARTS, Rank.QUEEN); //Same strength

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.JACK); //Same strength
		Card card7 = new Card(Suit.CLUBS,Rank.FIVE); 
		Card card8 = new Card(Suit.CLUBS, Rank.EIGHT); 
		Card card9 = new Card(Suit.CLUBS, Rank.TEN); //Highest card
		Card card10 = new Card(Suit.CLUBS, Rank.QUEEN); //Same strength

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals(winnerHand.iterator().next().getPlayer(), player2);
		System.out.println("The fourth flush winner is="+winnerHand.iterator().next().getPlayer());
	}

	/*
	 * This test case is where both players have all 5 cards of the same suit and 
	 * three cards with same strength so the player with fourth highest card wins
	 */
	@Test 
	public void when_flush_both_then_fourthHighestCardWins() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Roy";
		String player2 = "Richie";

		Card card1 = new Card(Suit.HEARTS, Rank.JACK); //Same strength
		Card card2 = new Card(Suit.HEARTS,Rank.TWO);
		Card card3 = new Card(Suit.HEARTS, Rank.EIGHT); //Highest card
		Card card4 = new Card(Suit.HEARTS, Rank.TEN); //Same strength
		Card card5 = new Card(Suit.HEARTS, Rank.QUEEN); //Same strength

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.JACK); //Same strength
		Card card7 = new Card(Suit.CLUBS,Rank.FIVE); 
		Card card8 = new Card(Suit.CLUBS, Rank.SIX); 
		Card card9 = new Card(Suit.CLUBS, Rank.TEN); //Same strength
		Card card10 = new Card(Suit.CLUBS, Rank.QUEEN); //Same strength

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals(winnerHand.iterator().next().getPlayer(), player1);
		System.out.println("The fifth flush winner is="+winnerHand.iterator().next().getPlayer());
	}

	/*
	 * This test case is where both players have all 5 cards of the same suit and 
	 * four cards with same strength so the player with fifth highest card wins
	 */
	@Test 
	public void when_flush_both_then_fifthHighestCardWins() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Roy";
		String player2 = "Richie";

		Card card1 = new Card(Suit.HEARTS, Rank.JACK); //Same strength
		Card card2 = new Card(Suit.HEARTS,Rank.TWO);
		Card card3 = new Card(Suit.HEARTS, Rank.EIGHT); //Same strength
		Card card4 = new Card(Suit.HEARTS, Rank.TEN); //Same strength
		Card card5 = new Card(Suit.HEARTS, Rank.QUEEN); //Same strength

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.JACK); //Same strength
		Card card7 = new Card(Suit.CLUBS,Rank.FIVE); //Highest card
		Card card8 = new Card(Suit.CLUBS, Rank.EIGHT); //Same strength
		Card card9 = new Card(Suit.CLUBS, Rank.TEN); //Same strength
		Card card10 = new Card(Suit.CLUBS, Rank.QUEEN); //Same strength

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals(winnerHand.iterator().next().getPlayer(), player2);
		System.out.println("The sixth flush winner is="+winnerHand.iterator().next().getPlayer());
	}

	/*
	 * This test case is where Player1 and Palyer2 both have all 5 cards same suit and all
	 * cards are the same strength so both are winners by Flush
	 */
	@Test 
	public void when_flush_sameStrength_then_bothPlayersWin() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Jack";
		String player2 = "Sam";

		Card card1 = new Card(Suit.HEARTS, Rank.THREE); 
		Card card2 = new Card(Suit.HEARTS,Rank.FOUR);
		Card card3 = new Card(Suit.HEARTS, Rank.NINE); //All same strength
		Card card4 = new Card(Suit.HEARTS, Rank.SIX); 
		Card card5 = new Card(Suit.HEARTS, Rank.QUEEN);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.THREE); 
		Card card7 = new Card(Suit.CLUBS,Rank.FOUR); 
		Card card8 = new Card(Suit.CLUBS, Rank.NINE); //All same strength
		Card card9 = new Card(Suit.CLUBS, Rank.SIX); 
		Card card10 = new Card(Suit.CLUBS, Rank.QUEEN);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals(winnerHand.size(), 2);
		System.out.println("The size of winnerHand in both players win="+winnerHand.size());
	}

	/*
	 * This is the test case where one player has 3 of A kind and the other player has
	 * normal cards in the hand then player1 wins
	 */
	@Test 
	public void when_threeOfAKind_then_singlePlayerWins() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Mona";
		String player2 = "Fiona";

		Card card1 = new Card(Suit.HEARTS, Rank.EIGHT); // 3 of a kind
		Card card2 = new Card(Suit.CLUBS,Rank.ACE);
		Card card3 = new Card(Suit.CLUBS, Rank.TWO); 
		Card card4 = new Card(Suit.DIAMONDS, Rank.EIGHT); // 3 of a kind
		Card card5 = new Card(Suit.SPADES, Rank.EIGHT); // 3 of a kind

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.HEARTS, Rank.ACE); 
		Card card7 = new Card(Suit.DIAMONDS,Rank.THREE); 
		Card card8 = new Card(Suit.HEARTS, Rank.KING); 
		Card card9 = new Card(Suit.CLUBS, Rank.QUEEN); //No 3 of a kind cards
		Card card10 = new Card(Suit.SPADES, Rank.SIX);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals(winnerHand.iterator().next().getPlayer(), player1);
		System.out.println("The 3 of a kind only winner is="+winnerHand.iterator().next().getPlayer());		
	}
	
	/*
	 * This is the test case where both the players have 3 of A kind and the player having
	 * higher value 3 of A kind wins
	 */
	@Test 
	public void when_threeOfAKind_then_highestValueWins() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Mona";
		String player2 = "Fiona";

		Card card1 = new Card(Suit.HEARTS, Rank.EIGHT); //smaller 3 of a kind
		Card card2 = new Card(Suit.CLUBS,Rank.ACE);
		Card card3 = new Card(Suit.CLUBS, Rank.TWO); 
		Card card4 = new Card(Suit.DIAMONDS, Rank.EIGHT); //smaller 3 of a kind
		Card card5 = new Card(Suit.SPADES, Rank.EIGHT); //smaller 3 of a kind

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.HEARTS, Rank.ACE); 
		Card card7 = new Card(Suit.DIAMONDS,Rank.QUEEN); //Higher 3 of a kind cards
		Card card8 = new Card(Suit.HEARTS, Rank.KING); 
		Card card9 = new Card(Suit.CLUBS, Rank.QUEEN); //Higher 3 of a kind cards
		Card card10 = new Card(Suit.SPADES, Rank.QUEEN); //Higher 3 of a kind cards

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals(winnerHand.iterator().next().getPlayer(), player2);
		System.out.println("The 3 of a kind higher value winner is="+winnerHand.iterator().next().getPlayer());		
	}	

	/*
	 * This test case is where player1 has one pair and the other player2 has no pair cards 
	 * then player 1 wins
	 */
	@Test 
	public void when_onePair_then_singlePlayerWins() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Tim";
		String player2 = "Tom";

		Card card1 = new Card(Suit.HEARTS, Rank.THREE); 
		Card card2 = new Card(Suit.DIAMONDS,Rank.FOUR);
		Card card3 = new Card(Suit.CLUBS, Rank.NINE); //One pair
		Card card4 = new Card(Suit.DIAMONDS, Rank.NINE); //One pair
		Card card5 = new Card(Suit.HEARTS, Rank.QUEEN);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.FIVE); 
		Card card7 = new Card(Suit.DIAMONDS,Rank.SEVEN); 
		Card card8 = new Card(Suit.HEARTS, Rank.TWO); //No one pair
		Card card9 = new Card(Suit.SPADES, Rank.SIX); 
		Card card10 = new Card(Suit.SPADES, Rank.KING);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals(winnerHand.iterator().next().getPlayer(), player1);
		System.out.println("The one pair winner is="+winnerHand.iterator().next().getPlayer());
	}

	/*
	 * This test case is where both players have same strength of one pair each and
	 * rest of the cards are also of same strength then both players win
	 */
	@Test 
	public void when_onePair_sameStrength_then_bothPlayersWin() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "James";
		String player2 = "Joy";

		Card card1 = new Card(Suit.HEARTS, Rank.FIVE); 
		Card card2 = new Card(Suit.CLUBS,Rank.SEVEN);
		Card card3 = new Card(Suit.CLUBS, Rank.NINE); //Same one pair
		Card card4 = new Card(Suit.DIAMONDS, Rank.NINE); //Same one pair
		Card card5 = new Card(Suit.HEARTS, Rank.QUEEN);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.FIVE); 
		Card card7 = new Card(Suit.DIAMONDS,Rank.SEVEN); 
		Card card8 = new Card(Suit.HEARTS, Rank.NINE); //Same one pair
		Card card9 = new Card(Suit.SPADES, Rank.NINE); //Same one pair
		Card card10 = new Card(Suit.SPADES, Rank.QUEEN);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals(winnerHand.size(), 2);
		System.out.println("The same winners="+winnerHand.size());
	}	

	/*
	 * This test case is where both players have different one pair each and the player 
	 * with higher strength one par card wins
	 */
	@Test 
	public void when_onePair_then_highestValueCardWins() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Mary";
		String player2 = "Mark";

		Card card1 = new Card(Suit.HEARTS, Rank.TWO); 
		Card card2 = new Card(Suit.CLUBS,Rank.JACK);
		Card card3 = new Card(Suit.CLUBS, Rank.NINE); //Lower one pair
		Card card4 = new Card(Suit.DIAMONDS, Rank.NINE); //Lower one pair
		Card card5 = new Card(Suit.HEARTS, Rank.FOUR);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.ACE); 
		Card card7 = new Card(Suit.DIAMONDS,Rank.THREE); 
		Card card8 = new Card(Suit.HEARTS, Rank.KING); //Higher one pair
		Card card9 = new Card(Suit.SPADES, Rank.KING); //Higher one pair
		Card card10 = new Card(Suit.SPADES, Rank.SIX);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals(winnerHand.iterator().next().getPlayer(), player2);
		System.out.println("The one pair higher card winner is="+winnerHand.iterator().next().getPlayer());		
	}

	/*
	 * This test case is where both players have same strength one pair cards and the 
	 * player with first higher kicker card wins
	 */
	@Test 
	public void when_onePair_then_firstKickerCardWins() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Marc";
		String player2 = "Mary";

		Card card1 = new Card(Suit.HEARTS, Rank.TWO); 
		Card card2 = new Card(Suit.CLUBS,Rank.JACK);
		Card card3 = new Card(Suit.CLUBS, Rank.NINE); //Same one pair
		Card card4 = new Card(Suit.DIAMONDS, Rank.NINE); //Same one pair
		Card card5 = new Card(Suit.HEARTS, Rank.FOUR);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.ACE); // High kicker card
		Card card7 = new Card(Suit.DIAMONDS,Rank.THREE); 
		Card card8 = new Card(Suit.HEARTS, Rank.NINE); //Same one pair
		Card card9 = new Card(Suit.SPADES, Rank.NINE); //Same one pair
		Card card10 = new Card(Suit.SPADES, Rank.SIX);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals(winnerHand.iterator().next().getPlayer(), player2);
		System.out.println("The one pair first higher card winner is="+winnerHand.iterator().next().getPlayer());		
	}

	/*
	 * This test case is where both players have same strength one pair cards and the first
	 * kicker card is also equal then player with second high kicker card wins
	 */
	@Test 
	public void when_onePair_then_secondKickerCardWins() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Jason";
		String player2 = "Jerry";

		Card card1 = new Card(Suit.HEARTS, Rank.ACE); //Same strength
		Card card2 = new Card(Suit.CLUBS,Rank.JACK); // High kicker card
		Card card3 = new Card(Suit.CLUBS, Rank.NINE); //Same one pair
		Card card4 = new Card(Suit.DIAMONDS, Rank.NINE); //Same one pair
		Card card5 = new Card(Suit.HEARTS, Rank.FOUR);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.ACE); //Same strength
		Card card7 = new Card(Suit.DIAMONDS,Rank.THREE); 
		Card card8 = new Card(Suit.HEARTS, Rank.NINE); //Same one pair
		Card card9 = new Card(Suit.SPADES, Rank.NINE); //Same one pair
		Card card10 = new Card(Suit.SPADES, Rank.SIX);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals(winnerHand.iterator().next().getPlayer(), player1);
		System.out.println("The one pair second higher card winner is="+winnerHand.iterator().next().getPlayer());		
	}

	/*
	 * This test case is where both players have same strength one pair cards and same 
	 * strength first and second kicker cards then player with third high kicker card wins
	 */
	@Test 
	public void when_onePair_then_thirdKickerCardWins() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Jason";
		String player2 = "Jerry";

		Card card1 = new Card(Suit.HEARTS, Rank.ACE); //Same strength
		Card card2 = new Card(Suit.CLUBS,Rank.JACK); // High kicker card
		Card card3 = new Card(Suit.CLUBS, Rank.NINE); //Same one pair
		Card card4 = new Card(Suit.DIAMONDS, Rank.NINE); //Same one pair
		Card card5 = new Card(Suit.HEARTS, Rank.FOUR);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.ACE); //Same strength
		Card card7 = new Card(Suit.DIAMONDS,Rank.JACK); //Same strength
		Card card8 = new Card(Suit.HEARTS, Rank.NINE); //Same one pair
		Card card9 = new Card(Suit.SPADES, Rank.NINE); //Same one pair
		Card card10 = new Card(Suit.SPADES, Rank.SIX); //High kicker value

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals(winnerHand.iterator().next().getPlayer(), player2);
		System.out.println("The one pair third higher card winner is="+winnerHand.iterator().next().getPlayer());		
	}
	
	
	
	/*
	 * This is the test case where player1 has a higher value card in the hand than the
	 * other player which makes player1 the winner.
	 */
	@Test 
	public void when_highCard_then_onePlayerWins() {
		
		//Prepare		
		Set<Hand> winnerHand =null;
		String player1 = "Tony";
		String player2 = "Kaleb";
		
		Card card1 = new Card(Suit.HEARTS, Rank.SEVEN); 
		Card card2 = new Card(Suit.SPADES, Rank.THREE);
		Card card3 = new Card(Suit.SPADES, Rank.ACE); //High value card
		Card card4 = new Card(Suit.SPADES, Rank.FOUR); 
		Card card5 = new Card(Suit.DIAMONDS, Rank.FIVE);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.FIVE); 
		Card card7 = new Card(Suit.DIAMONDS,Rank.SEVEN); 
		Card card8 = new Card(Suit.HEARTS, Rank.TWO); 
		Card card9 = new Card(Suit.SPADES, Rank.NINE); 
		Card card10 = new Card(Suit.SPADES, Rank.QUEEN);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals("The Single High Card winner is",winnerHand.iterator().next().getPlayer(), player1);
		System.out.println("The high card only winner is="+winnerHand.iterator().next().getPlayer());
	}
	
	/*
	 * This is the test case where both the players have same first higher value card 
	 * in the hand than the player with second kicker value card wins
	 */
	@Test 
	public void when_highCard_then_secondKickerCardWins() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Tony";
		String player2 = "Kaleb";
		
		Card card1 = new Card(Suit.HEARTS, Rank.SEVEN); 
		Card card2 = new Card(Suit.SPADES, Rank.THREE);
		Card card3 = new Card(Suit.SPADES, Rank.ACE); //Same High value card
		Card card4 = new Card(Suit.SPADES, Rank.FOUR); 
		Card card5 = new Card(Suit.DIAMONDS, Rank.FIVE);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.FIVE); 
		Card card7 = new Card(Suit.DIAMONDS,Rank.SEVEN); 
		Card card8 = new Card(Suit.HEARTS, Rank.ACE); //Same High value card
		Card card9 = new Card(Suit.SPADES, Rank.NINE); 
		Card card10 = new Card(Suit.SPADES, Rank.QUEEN); //Second kicker value card

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals("The Single High Card winner is",winnerHand.iterator().next().getPlayer(), player2);
		System.out.println("The high card same second kicker winner is="+winnerHand.iterator().next().getPlayer());
	}
	
	/*
	 * This is the test case where both the players have same first and second higher value cards 
	 * in the hand than the player with third kicker value card wins
	 */
	@Test 
	public void when_highCard_then_thirdKickerCardWins() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Dean";
		String player2 = "Pat";
		
		Card card1 = new Card(Suit.HEARTS, Rank.TWO); 
		Card card2 = new Card(Suit.SPADES, Rank.THREE);
		Card card3 = new Card(Suit.SPADES, Rank.ACE); //Same High value card
		Card card4 = new Card(Suit.SPADES, Rank.NINE); // Third kicker value card
		Card card5 = new Card(Suit.DIAMONDS, Rank.QUEEN); //Same High value card

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.FIVE); 
		Card card7 = new Card(Suit.DIAMONDS,Rank.SEVEN); 
		Card card8 = new Card(Suit.HEARTS, Rank.ACE); //Same High value card
		Card card9 = new Card(Suit.SPADES, Rank.SIX); 
		Card card10 = new Card(Suit.SPADES, Rank.QUEEN); //Same High value card

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals("The Single High Card winner is",winnerHand.iterator().next().getPlayer(), player1);
		System.out.println("The high card same third kicker winner is="+winnerHand.iterator().next().getPlayer());
	}
	
	/*
	 * This is the test case where both the players have same first, second and third higher 
	 * value cards in the hand than the player with fourth kicker value card wins
	 */
	@Test 
	public void when_highCard_then_fourthKickerCardWins() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Dean";
		String player2 = "Pat";
		
		Card card1 = new Card(Suit.HEARTS, Rank.TWO); 
		Card card2 = new Card(Suit.SPADES, Rank.THREE);
		Card card3 = new Card(Suit.SPADES, Rank.ACE); //Same High value card
		Card card4 = new Card(Suit.SPADES, Rank.NINE); //Same High value card
		Card card5 = new Card(Suit.DIAMONDS, Rank.QUEEN); //Same High value card

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.FIVE); 
		Card card7 = new Card(Suit.DIAMONDS,Rank.SEVEN); // Fourth kicker value card
		Card card8 = new Card(Suit.HEARTS, Rank.ACE); //Same High value card
		Card card9 = new Card(Suit.HEARTS, Rank.NINE); //Same High value card
		Card card10 = new Card(Suit.SPADES, Rank.QUEEN); //Same High value card

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals("The Single High Card winner is",winnerHand.iterator().next().getPlayer(), player2);
		System.out.println("The high card same fourth kicker winner is="+winnerHand.iterator().next().getPlayer());
	}
	
	/*
	 * This is the test case where both the players have same first, second,third and fourth higher 
	 * value cards in the hand than the player with fifth kicker value card wins
	 */
	@Test 
	public void when_highCard_then_fifthKickerCardWins() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Peter";
		String player2 = "Simon";
		
		Card card1 = new Card(Suit.HEARTS, Rank.FIVE); //Fifth kicker value card
		Card card2 = new Card(Suit.SPADES, Rank.SEVEN); //Same High value card
		Card card3 = new Card(Suit.SPADES, Rank.ACE); //Same High value card
		Card card4 = new Card(Suit.SPADES, Rank.NINE); //Same High value card
		Card card5 = new Card(Suit.DIAMONDS, Rank.QUEEN); //Same High value card

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.TWO); 
		Card card7 = new Card(Suit.DIAMONDS,Rank.SEVEN); //Same High value card
		Card card8 = new Card(Suit.HEARTS, Rank.ACE); //Same High value card
		Card card9 = new Card(Suit.HEARTS, Rank.NINE); //Same High value card
		Card card10 = new Card(Suit.SPADES, Rank.QUEEN); //Same High value card

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals("The Single High Card winner is",winnerHand.iterator().next().getPlayer(), player1);
		System.out.println("The high card same fifth kicker winner is="+winnerHand.iterator().next().getPlayer());
	}
	
	/*
	 * This is the test case where both the players have same first, second,third and fourth higher 
	 * value cards in the hand than the player with fifth kicker value card wins
	 */
	@Test 
	public void when_highCard_sameValue_then_bothPlayersWin() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Peter";
		String player2 = "Simon";
		
		Card card1 = new Card(Suit.HEARTS, Rank.FIVE); //Same High value card
		Card card2 = new Card(Suit.SPADES, Rank.SEVEN); //Same High value card
		Card card3 = new Card(Suit.SPADES, Rank.ACE); //Same High value card
		Card card4 = new Card(Suit.SPADES, Rank.NINE); //Same High value card
		Card card5 = new Card(Suit.DIAMONDS, Rank.QUEEN); //Same High value card

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.FIVE); //Same High value card
		Card card7 = new Card(Suit.DIAMONDS,Rank.SEVEN); //Same High value card
		Card card8 = new Card(Suit.HEARTS, Rank.ACE); //Same High value card
		Card card9 = new Card(Suit.HEARTS, Rank.NINE); //Same High value card
		Card card10 = new Card(Suit.SPADES, Rank.QUEEN); //Same High value card

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);

		//Assert
		assertEquals(winnerHand.size(), 2);
		System.out.println("The same winners for high card="+winnerHand.size());
	}
	
	/*********************************************************************************/
	/*********************NON FUNCTIONAL TEST CASES ***********************************/
	/**********************************************************************************/	
	
	/*
	 * This test case is where Player1 has duplicate cards in the hand which causes
	 * exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void duplicateCards_samePlayer() {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Jack";
		String player2 = "Sam";

		Card card1 = new Card(Suit.HEARTS, Rank.QUEEN); //Duplicate
		Card card2 = new Card(Suit.DIAMONDS,Rank.FOUR);
		Card card3 = new Card(Suit.CLUBS, Rank.NINE); 
		Card card4 = new Card(Suit.DIAMONDS, Rank.SIX); 
		Card card5 = new Card(Suit.HEARTS, Rank.QUEEN); //Duplicate

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.FIVE); 
		Card card7 = new Card(Suit.DIAMONDS,Rank.SEVEN); 
		Card card8 = new Card(Suit.HEARTS, Rank.NINE); 
		Card card9 = new Card(Suit.SPADES, Rank.NINE); 
		Card card10 = new Card(Suit.SPADES, Rank.QUEEN);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>();
		hands.add(hand1);
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);
	}

	/*
	 * This test case is where Player1 and Player2 have duplicate cards in each of their 
	 * hands which causes exception
	 */
	@Test(expected = IllegalArgumentException.class) 
	public void duplicateCards_acrossMultiplePlayers()  {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Jack";
		String player2 = "Sam";

		Card card1 = new Card(Suit.HEARTS,Rank.THREE); 
		Card card2 = new Card(Suit.DIAMONDS,Rank.FOUR); 
		Card card3 = new Card(Suit.CLUBS, Rank.NINE);
		Card card4 = new Card(Suit.DIAMONDS, Rank.NINE);//Duplicate
		Card card5 = new Card(Suit.HEARTS, Rank.QUEEN); 

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.FIVE);
		Card card7 = new Card(Suit.DIAMONDS,Rank.SEVEN);
		Card card8 = new Card(Suit.HEARTS,Rank.NINE); 
		Card card9 = new Card(Suit.DIAMONDS, Rank.NINE); //Duplicate
		Card card10 = new Card(Suit.SPADES, Rank.QUEEN); 

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>(); 
		hands.add(hand1); 
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);
	}

	/*
	 * This test case is where Player1 has less than five cards in the hand which 
	 * causes exception
	 */
	@Test(expected = NullPointerException.class)
	public void lessCards_onePlayer()  {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Jack";
		String player2 = "Sam";

		Card card1 = new Card(Suit.HEARTS,Rank.THREE); 
		Card card2 = new Card(Suit.DIAMONDS,Rank.FOUR); 
		Card card3 = new Card(Suit.CLUBS, Rank.NINE);
		Card card4 = new Card(Suit.DIAMONDS, Rank.NINE);			

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, null);

		Card card6 = new Card(Suit.CLUBS, Rank.FIVE);
		Card card7 = new Card(Suit.DIAMONDS,Rank.SEVEN);
		Card card8 = new Card(Suit.HEARTS,Rank.NINE); 
		Card card9 = new Card(Suit.SPADES, Rank.NINE); 
		Card card10 = new Card(Suit.SPADES, Rank.QUEEN); 

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		Set<Hand> hands = new HashSet<Hand>(); 
		hands.add(hand1); 
		hands.add(hand2);

		//Act
		winnerHand = Poker.play(hands); 
	}

	/*
	 * This test case is where both players have less than five cards in each of their hands which 
	 * causes exception
	 */
	@Test(expected = NullPointerException.class)
	public void lessCards_bothPlayers()  {
		
		//Prepare
		Set<Hand> winnerHand =null;
		String player1 = "Jack";
		String player2 = "Sam";

		Card card1 = new Card(Suit.HEARTS,Rank.THREE); 
		Card card2 = new Card(Suit.DIAMONDS,Rank.FOUR); 
		Card card3 = new Card(Suit.CLUBS, Rank.NINE);
		Card card4 = new Card(Suit.DIAMONDS, Rank.NINE);		

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, null);

		Card card6 = new Card(Suit.CLUBS, Rank.FIVE);
		Card card7 = new Card(Suit.DIAMONDS,Rank.SEVEN);
		Card card8 = new Card(Suit.HEARTS,Rank.NINE); 
		Card card9 = new Card(Suit.SPADES, Rank.NINE);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, null);

		Set<Hand> hands = new HashSet<Hand>(); 
		hands.add(hand1); 
		hands.add(hand2);
		
		//Act
		winnerHand = Poker.play(hands);
	}

}