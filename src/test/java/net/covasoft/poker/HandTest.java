package net.covasoft.poker;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class HandTest {

	/*********************************************************************************/
	/********************* FUNCTIONAL TEST CASES *************************************/
	/**********************************************************************************/
	
	//@Test
	public void hand_tests() {
		//Assert.fail("Implement your test cases for the Hand class here");
	}	

	/*
	 * This is the test case where we both players have flush hand
	 */
	@Test
	public void when_flush_sameStrength_bothPlayers() {

		//Prepare
		String player1 = "Joe";
		String player2 = "Jen";

		Card card1 = new Card(Suit.HEARTS, Rank.THREE);
		Card card2 = new Card(Suit.HEARTS,Rank.FOUR);
		Card card3 = new Card(Suit.HEARTS, Rank.NINE);
		Card card4 = new Card(Suit.HEARTS, Rank.ACE);
		Card card5 = new Card(Suit.HEARTS, Rank.QUEEN);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.THREE);
		Card card7 = new Card(Suit.CLUBS,Rank.FOUR);
		Card card8 = new Card(Suit.CLUBS, Rank.NINE);
		Card card9 = new Card(Suit.CLUBS, Rank.ACE);
		Card card10 = new Card(Suit.CLUBS, Rank.QUEEN);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		//Act
		int i = hand1.compareTo(hand2);

		//Assert
		assertEquals(hand1.compareTo(hand2), 0);
		//Assert.assertEquals("Card Set 1 is flush", hand1.compareTo(hand2), 0);
		System.out.println("The both hands flush="+i);
	}

	/*
	 * This is the test case where we player1 has a flush hand
	 */
	@Test
	public void when_flush_onePlayer() {

		//Prepare
		String player1 = "Joe";
		String player2 = "Jen";

		Card card1 = new Card(Suit.HEARTS, Rank.THREE);
		Card card2 = new Card(Suit.HEARTS,Rank.FOUR);
		Card card3 = new Card(Suit.HEARTS, Rank.NINE);
		Card card4 = new Card(Suit.HEARTS, Rank.ACE);
		Card card5 = new Card(Suit.HEARTS, Rank.QUEEN);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.FIVE);
		Card card7 = new Card(Suit.DIAMONDS,Rank.SEVEN);
		Card card8 = new Card(Suit.CLUBS, Rank.NINE);
		Card card9 = new Card(Suit.SPADES, Rank.NINE);
		Card card10 = new Card(Suit.SPADES, Rank.QUEEN);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		//Act
		int i = hand1.compareTo(hand2);

		//Assert
		assertEquals(hand1.compareTo(hand2), 1);	
		//Assert.assertEquals("Card Set 1 is flush", hand1.compareTo(hand2), 1);
		System.out.println("The one player flush="+i);
	}


	/*
	 * This is the test case where we player2 has a flush hand
	 */
	@Test
	public void when_flush_secondPlayer() {

		//Prepare
		String player1 = "Joe";
		String player2 = "Jen";

		Card card1 = new Card(Suit.HEARTS, Rank.TWO);
		Card card2 = new Card(Suit.DIAMONDS,Rank.THREE);
		Card card3 = new Card(Suit.CLUBS, Rank.FOUR);
		Card card4 = new Card(Suit.SPADES, Rank.FIVE);
		Card card5 = new Card(Suit.HEARTS, Rank.SIX);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.THREE);
		Card card7 = new Card(Suit.CLUBS,Rank.FOUR);
		Card card8 = new Card(Suit.CLUBS, Rank.NINE);
		Card card9 = new Card(Suit.CLUBS, Rank.ACE);
		Card card10 = new Card(Suit.CLUBS, Rank.QUEEN);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		//Act
		int i = hand1.compareTo(hand2);

		//Assert
		assertEquals(hand1.compareTo(hand2), -1);
		System.out.println("The second player flush="+i);
	}

	/*
	 * This test case is where player1 holds 3 of a kind cards
	 */
	@Test
	public void when_threeOfAKind_playerOneWins() {

		//Prepare
		String player1 = "Sam";
		String player2 = "Tom";
		
		Card card1 = new Card(Suit.CLUBS, Rank.EIGHT);
		Card card2 = new Card(Suit.HEARTS, Rank.ACE);
		Card card3 = new Card(Suit.SPADES, Rank.TWO);
		Card card4 = new Card(Suit.HEARTS, Rank.EIGHT);
		Card card5 = new Card(Suit.DIAMONDS, Rank.EIGHT);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.TWO);
		Card card7 = new Card(Suit.HEARTS, Rank.FOUR);
		Card card8 = new Card(Suit.SPADES, Rank.NINE);
		Card card9 = new Card(Suit.HEARTS, Rank.JACK);
		Card card10 = new Card(Suit.DIAMONDS, Rank.KING);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		//Act
		int i = hand1.compareTo(hand2);
		
		//Assert
		assertEquals(hand1.compareTo(hand2), 1);
		System.out.println("The 3 of a kind player1 wins="+i);
	}
	
	/*
	 * This test case is where player2 holds 3 of a kind cards
	 */
	@Test
	public void when_threeOfAKind_secondPlayerWins() {

		//Prepare
		String player1 = "Sam";
		String player2 = "Tom";
		
		Card card1 = new Card(Suit.CLUBS, Rank.TWO);
		Card card2 = new Card(Suit.HEARTS, Rank.ACE);
		Card card3 = new Card(Suit.SPADES, Rank.TWO);
		Card card4 = new Card(Suit.HEARTS, Rank.QUEEN);
		Card card5 = new Card(Suit.DIAMONDS, Rank.EIGHT);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.KING);
		Card card7 = new Card(Suit.HEARTS, Rank.FOUR);
		Card card8 = new Card(Suit.SPADES, Rank.KING);
		Card card9 = new Card(Suit.HEARTS, Rank.JACK);
		Card card10 = new Card(Suit.DIAMONDS, Rank.KING);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		//Act
		int i = hand1.compareTo(hand2);
		
		//Assert
		assertEquals(hand1.compareTo(hand2), -1);
		System.out.println("The 3 of a kind player2 wins="+i);
	}
	
	/*
	 * This test case is where both players hold same strength one pair cards
	 */
	@Test
	public void when_onePair_sameStrength_bothPlayers() {

		//Prepare
		String player1 = "Sam";
		String player2 = "Tom";
		
		Card card1 = new Card(Suit.CLUBS, Rank.TWO);
		Card card2 = new Card(Suit.HEARTS, Rank.ACE);
		Card card3 = new Card(Suit.SPADES, Rank.TWO);
		Card card4 = new Card(Suit.HEARTS, Rank.QUEEN);
		Card card5 = new Card(Suit.DIAMONDS, Rank.EIGHT);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.ACE);
		Card card7 = new Card(Suit.HEARTS, Rank.TWO);
		Card card8 = new Card(Suit.SPADES, Rank.QUEEN);
		Card card9 = new Card(Suit.DIAMONDS, Rank.TWO);
		Card card10 = new Card(Suit.SPADES, Rank.EIGHT);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		//Act
		int i = hand1.compareTo(hand2);
		
		//Assert
		assertEquals(hand1.compareTo(hand2), 0);
		System.out.println("The both players same strength one pair="+i);
	}
	
	/*
	 * This test case is where player1 holds one pair cards
	 */
	@Test
	public void when_onePair_player1Wins() {

		//Prepare
		String player1 = "Sam";
		String player2 = "Tom";
		
		Card card1 = new Card(Suit.CLUBS, Rank.TWO);
		Card card2 = new Card(Suit.HEARTS, Rank.ACE);
		Card card3 = new Card(Suit.SPADES, Rank.TWO);
		Card card4 = new Card(Suit.HEARTS, Rank.QUEEN);
		Card card5 = new Card(Suit.DIAMONDS, Rank.EIGHT);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.JACK);
		Card card7 = new Card(Suit.HEARTS, Rank.FOUR);
		Card card8 = new Card(Suit.SPADES, Rank.KING);
		Card card9 = new Card(Suit.HEARTS, Rank.TEN);
		Card card10 = new Card(Suit.DIAMONDS, Rank.THREE);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		//Act
		int i = hand1.compareTo(hand2);
		
		//Assert
		assertEquals(hand1.compareTo(hand2), 1);
		System.out.println("The one pair player1 wins="+i);
	}
	
	/*
	 * This test case is where player2 holds one pair cards
	 */
	@Test
	public void when_onePair_player2Wins() {

		//Prepare
		String player1 = "Sam";
		String player2 = "Tom";
		
		Card card1 = new Card(Suit.CLUBS, Rank.TWO);
		Card card2 = new Card(Suit.HEARTS, Rank.ACE);
		Card card3 = new Card(Suit.SPADES, Rank.SIX);
		Card card4 = new Card(Suit.HEARTS, Rank.QUEEN);
		Card card5 = new Card(Suit.DIAMONDS, Rank.EIGHT);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.JACK);
		Card card7 = new Card(Suit.HEARTS, Rank.FOUR);
		Card card8 = new Card(Suit.SPADES, Rank.KING);
		Card card9 = new Card(Suit.HEARTS, Rank.KING);
		Card card10 = new Card(Suit.DIAMONDS, Rank.THREE);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		//Act
		int i = hand1.compareTo(hand2);
		
		//Assert
		assertEquals(hand1.compareTo(hand2), -1);
		System.out.println("The one pair player2 wins="+i);
	}
	
	/*
	 * This test case is where both players hold same strength Higher value card
	 */
	@Test
	public void when_higherCard_sameStrength_bothPlayers() {

		//Prepare
		String player1 = "Sam";
		String player2 = "Tom";
		
		Card card1 = new Card(Suit.HEARTS, Rank.FIVE); 
		Card card2 = new Card(Suit.SPADES, Rank.SEVEN);
		Card card3 = new Card(Suit.SPADES, Rank.TWO); 
		Card card4 = new Card(Suit.CLUBS, Rank.NINE); 
		Card card5 = new Card(Suit.DIAMONDS, Rank.QUEEN);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.FIVE); 
		Card card7 = new Card(Suit.DIAMONDS,Rank.SEVEN); 
		Card card8 = new Card(Suit.HEARTS, Rank.TWO); 
		Card card9 = new Card(Suit.SPADES, Rank.NINE); 
		Card card10 = new Card(Suit.SPADES, Rank.QUEEN); 

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		//Act
		int i = hand1.compareTo(hand2);
		
		//Assert
		assertEquals(hand1.compareTo(hand2), 0);
		System.out.println("The Higher value same strength both wins="+i);
	}
	
	/*
	 * This test case is where player1 holds Higher value card
	 */
	@Test
	public void when_higherCard_player1Wins() {

		//Prepare
		String player1 = "Sam";
		String player2 = "Tom";
		
		Card card1 = new Card(Suit.HEARTS, Rank.SEVEN); 
		Card card2 = new Card(Suit.SPADES, Rank.THREE);
		Card card3 = new Card(Suit.SPADES, Rank.ACE); //High value card
		Card card4 = new Card(Suit.SPADES, Rank.FOUR); 
		Card card5 = new Card(Suit.DIAMONDS, Rank.FIVE);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.FIVE); 
		Card card7 = new Card(Suit.DIAMONDS,Rank.SEVEN); 
		Card card8 = new Card(Suit.HEARTS, Rank.TWO); 
		Card card9 = new Card(Suit.SPADES, Rank.NINE); 
		Card card10 = new Card(Suit.SPADES, Rank.QUEEN);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		//Act
		int i = hand1.compareTo(hand2);
		
		//Assert
		assertEquals(hand1.compareTo(hand2), 1);
		System.out.println("The Higher value card player1 wins="+i);
	}
	
	/*
	 * This test case is where player2 holds Higher value card
	 */
	@Test
	public void when_higherCard_player2Wins() {

		//Prepare
		String player1 = "Sam";
		String player2 = "Tom";
		
		Card card1 = new Card(Suit.HEARTS, Rank.SEVEN); 
		Card card2 = new Card(Suit.SPADES, Rank.THREE);
		Card card3 = new Card(Suit.SPADES, Rank.JACK); 
		Card card4 = new Card(Suit.SPADES, Rank.FOUR); 
		Card card5 = new Card(Suit.DIAMONDS, Rank.FIVE);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.FIVE); 
		Card card7 = new Card(Suit.DIAMONDS,Rank.SEVEN); 
		Card card8 = new Card(Suit.HEARTS, Rank.TWO); 
		Card card9 = new Card(Suit.SPADES, Rank.NINE); 
		Card card10 = new Card(Suit.SPADES, Rank.QUEEN); //High value card

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);

		//Act
		int i = hand1.compareTo(hand2);
		
		//Assert
		assertEquals(hand1.compareTo(hand2), -1);
		System.out.println("The Higher value card player2 wins="+i);
	}
	
	/*********************************************************************************/
	/*********************NON FUNCTIONAL TEST CASES ***********************************/
	/**********************************************************************************/
	
	/*
	 * This is the test case where we create Hand object and verify its instance creation
	 */
	@Test
	public void create_hand() {

		//Prepare
		String player ="Joe";
		Card card1 = new Card(Suit.HEARTS, Rank.THREE);
		Card card2 = new Card(Suit.HEARTS,Rank.FOUR);
		Card card3 = new Card(Suit.HEARTS, Rank.NINE);
		Card card4 = new Card(Suit.HEARTS, Rank.ACE);
		Card card5 = new Card(Suit.HEARTS, Rank.QUEEN);

		//Act
		Hand hand1 = new Hand(player, card1, card2, card3, card4, card5);

		//Assert
		Assert.assertEquals(hand1 instanceof Hand , true);
		System.out.println("Hand object created");
	}

	/*
	 * This is the test case where we check for duplicate cards in same hand
	 */
	@Test(expected = IllegalArgumentException.class)
	public void create_duplicateCardsInHand() {

		//Prepare
		String player = "Joe";
		
		Card card1 = new Card(Suit.HEARTS, Rank.THREE);
		Card card2 = new Card(Suit.HEARTS,Rank.FOUR);
		Card card3 = new Card(Suit.HEARTS, Rank.NINE);
		Card card4 = new Card(Suit.HEARTS, Rank.ACE);
		Card card5 = new Card(Suit.HEARTS, Rank.THREE);

		//Act
		Hand hand1 = new Hand(player, card1, card2, card3, card4, card5);
	}

	/*
	 * This is the test case where we check for null card values passed in a Hand
	 */
	@Test(expected = NullPointerException.class)
	public void create_nullCard_inHand() {

		//Prepare		
		String player = "Joe";
		
		Card card1 = new Card(Suit.HEARTS, Rank.THREE);
		Card card2 = new Card(Suit.HEARTS,Rank.FOUR);
		Card card3 = new Card(Suit.HEARTS, Rank.NINE);
		Card card4 = new Card(Suit.HEARTS, Rank.ACE);

		//Act
		Hand hand1 = new Hand(player, card1, card2, card3, card4, null);
	}
	
	/*
	 * This is the test case where we try to retrieve the player name
	 */
	@Test
	public void getPlayerName() {

		//Prepare
		String player = "Sam";
		String checkPlayer ="";
		
		Card card1 = new Card(Suit.HEARTS, Rank.THREE);
		Card card2 = new Card(Suit.HEARTS,Rank.FOUR);
		Card card3 = new Card(Suit.HEARTS, Rank.NINE);
		Card card4 = new Card(Suit.HEARTS, Rank.ACE);
		Card card5 = new Card(Suit.HEARTS, Rank.QUEEN);
		
		Hand hand1 = new Hand(player, card1, card2, card3, card4, card5);

		//Act		
		checkPlayer = hand1.getPlayer();

		//Assert
		Assert.assertEquals(checkPlayer, player);
		System.out.println("The player passed="+checkPlayer);
	}
	
	/*
	 * This is the test case where we try to retrieve the cards passed in the hand
	 */
	@Test
	public void getCardsPassedInHand() {

		//Prepare
		String player = "Sam";
		Set<Card> originalCards = new HashSet<Card>();
		Set<Card> cardsReturned = null;
		boolean compareCardSets = false;
		
		Card card1 = new Card(Suit.HEARTS, Rank.THREE);
		Card card2 = new Card(Suit.HEARTS,Rank.FOUR);
		Card card3 = new Card(Suit.HEARTS, Rank.NINE);
		Card card4 = new Card(Suit.HEARTS, Rank.ACE);
		Card card5 = new Card(Suit.HEARTS, Rank.QUEEN);		

		Hand hand1 = new Hand(player, card1, card2, card3, card4, card5);
		
		//Act
		originalCards.add(card1);
		originalCards.add(card2);
		originalCards.add(card3);
		originalCards.add(card4);
		originalCards.add(card5);
		
		cardsReturned = hand1.getCards();
		
		compareCardSets = cardsReturned.equals(originalCards);

		//Assert
		Assert.assertTrue(compareCardSets);
		System.out.println("The comapare value="+ compareCardSets);
	}
	
	/*
	 * This is the test case where we check hand with the same hand
	 */
	@Test
	public void testHandIsEqual() {

		//Prepare
		String player ="Joe";
		boolean compareSameHand = false;
		
		Card card1 = new Card(Suit.HEARTS, Rank.THREE);
		Card card2 = new Card(Suit.HEARTS,Rank.FOUR);
		Card card3 = new Card(Suit.HEARTS, Rank.NINE);
		Card card4 = new Card(Suit.HEARTS, Rank.ACE);
		Card card5 = new Card(Suit.HEARTS, Rank.QUEEN);
		
		Hand hand1 = new Hand(player, card1, card2, card3, card4, card5);

		//Act		
		compareSameHand = hand1.equals(hand1);

		//Assert
		Assert.assertEquals(compareSameHand , true);
		System.out.println("Comapre hand1 with hand1="+ compareSameHand);
	}
	
	/*
	 * This is the test case where we check hand1 with hand2
	 */
	@Test
	public void testHandWithOtherHand() {

		//Prepare
		String player1 ="Joe";
		String player2= "Jim";
		boolean compareHands = false;
		
		Card card1 = new Card(Suit.HEARTS, Rank.SEVEN); 
		Card card2 = new Card(Suit.SPADES, Rank.THREE);
		Card card3 = new Card(Suit.SPADES, Rank.ACE); //High value card
		Card card4 = new Card(Suit.SPADES, Rank.FOUR); 
		Card card5 = new Card(Suit.DIAMONDS, Rank.FIVE);

		Hand hand1 = new Hand(player1, card1, card2, card3, card4, card5);

		Card card6 = new Card(Suit.CLUBS, Rank.FIVE); 
		Card card7 = new Card(Suit.DIAMONDS,Rank.SEVEN); 
		Card card8 = new Card(Suit.HEARTS, Rank.TWO); 
		Card card9 = new Card(Suit.SPADES, Rank.NINE); 
		Card card10 = new Card(Suit.SPADES, Rank.QUEEN);

		Hand hand2 = new Hand(player2, card6, card7, card8, card9, card10);
		
		//Act		
		compareHands = hand1.equals(hand2);

		//Assert
		Assert.assertEquals(compareHands , false);
		System.out.println("Comapre hand1 with hand2="+ compareHands);
	}
}